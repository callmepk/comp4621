import sys
from datetime import datetime
from enum import Enum

''' Responses '''

TUNNEL_ESTABLISHED_BY_PROXY = b'\r\n'.join([
    b'HTTP/1.1 200 Connection established',
    b'\r\n'
])

BAD_GATEWAY = b'\r\n'.join([
    b'HTTP/1.1 502 Bad Gateway',
    b'Content-Length: 11',
    b'Connection: close',
    b'\r\n'
]) + b'Bad Gateway'

NOT_FOUND_BY_PROXY = b'\r\n'.join([
    b'HTTP/1.1 404 Not Found',
    b'Content-Length: 22',
    b'Connection: close',
    b'\r\n'
]) + b'Denied by Proxy Server'


''' Enums '''


class HTTPStates(Enum):
    INITIALIZED = 1
    LINE_RCVD = 2
    RCVING_HEADERS = 3
    HEADERS_COMPLETE = 4
    RCVING_BODY = 5
    COMPLETE = 6


class HTTPTypes(Enum):
    REQUEST = 1
    RESPONSE = 2


'''  Text Handlers  '''


def Txt(s, encoding='utf-8'):
    return s.decode(encoding, 'strict') if isinstance(s, bytes) else s


def Bts(s, encoding='utf-8'):
    return s.encode(encoding, 'strict') if isinstance(s, str) else s


''' Blacklist reader '''


def black_load(filename):
    blf = open(filename, 'r')
    lines = blf.read().splitlines()
    blf.close()
    return lines


''' Printer '''


def printer(infotype, info):
    print(str(datetime.utcnow().strftime('%H:%M:%S')),
          " [", infotype, "] ", info)


'''  Exceptions  '''


class ProxyConnFailed(Exception):

    def __init__(self, host, port, reason):
        self.host = host
        self.port = port
        self.reason = reason

    def __str__(self):
        return 'ProxyConnFailed - %s:%s - %s' % (self.host, self.port, self.reason)


''' terminal handler '''


def signal_handler(signal, frame):
    printer("INFO", "Proxy server terminated. Goodbye.")
    sys.exit(0)
