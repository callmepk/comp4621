import errno
import helper
import socket


class Conn(object):

    def __init__(self, side):
        self.conn = None
        self.buffer = b''
        self.closed = False
        self.side = side

    def send(self, data):
        return self.conn.send(data)

    def recv(self):
        try:
            data = self.conn.recv(8192)
            return data if len(data) != 0 else None
        except Exception as e:
            if e.errno != errno.ECONNRESET:
                helper.printer("EXPT",
                               'Exception when receive from connection %s %r : %r' % (self.side, self.conn, e))
            return None

    def close(self):
        self.conn.close()
        self.closed = True

    def buffer_size(self):
        return len(self.buffer)

    def has_buffer(self):
        return self.buffer_size() > 0

    def queue(self, data):
        self.buffer += data

    def flush(self):
        sent = self.send(self.buffer)
        self.buffer = self.buffer[sent:]


class Server(Conn):

    def __init__(self, host, port):
        super(Server, self).__init__(b'server')
        self.addr = (host, int(port))

    def __del__(self):
        if self.conn:
            self.close()

    def connect(self):
        self.conn = socket.create_connection((self.addr[0], self.addr[1]))


class Client(Conn):

    def __init__(self, conn, addr):
        super(Client, self).__init__(b'client')
        self.conn = conn
        self.addr = addr
