
import select
import signal
import socket
import sys
import os
from datetime import datetime
from threading import Thread

import connector
import helper
import hparser


class Proxy(Thread):

    def __init__(self, client, blacklist):
        super(Proxy, self).__init__()

        self.start_time = datetime.utcnow()
        self.last_activity = self.start_time

        self.client = client
        self.server = None

        self.request = hparser.Parser(helper.HTTPTypes.REQUEST)
        self.response = hparser.Parser(helper.HTTPTypes.RESPONSE)

        self.blacklist = helper.black_load(blacklist)

    def is_on_blacklist(self, host):
        # check whether the url is on the blacklist
        try:
            return any(bl == helper.Txt(host) for bl in self.blacklist)
        except Exception:
            return False

    def handle_request(self, data):
        if self.server and not self.server.closed:
            self.server.queue(data)
            return

        # parse http request
        self.request.parse(data)

        if self.request.state == helper.HTTPStates.COMPLETE:

            # get host address and port
            # HTTPS
            if self.request.method == b'CONNECT':
                host, port = self.request.url.path.split(b':')
            # HTTP
            elif self.request.url:
                host, port = self.request.url.hostname, self.request.url.port if self.request.url.port else 80
            else:
                raise Exception('Invalid request\n%s' % self.request.raw)

            self.server = connector.Server(host, port)
            
            try:
                self.server.connect()
            except Exception as e:
                self.server.closed = True
                raise helper.ProxyConnFailed(host, port, repr(e))

            if self.is_on_blacklist(host):
                self.client.queue(helper.NOT_FOUND_BY_PROXY)
                helper.printer("WARN",
                               "URL '%s' is on blacklist; Connection denied" % helper.Txt(host))
            elif self.request.method == b'CONNECT':
                self.client.queue(helper.TUNNEL_ESTABLISHED_BY_PROXY)
            else:
                nreq = self.request.nreq()
                self.server.queue(nreq)

    def handle_response(self, data):
        # parse incoming http response packet
        if not self.request.method == b'CONNECT':
            self.response.parse(data)

        # queue data for client
        self.client.queue(data)

    def log(self):
        host, port = self.server.addr if self.server else (None, None)
        if self.request.method == b'CONNECT':
            helper.printer("INFO",
                           'HTTPS | %s:%s | %s' % (helper.Txt(host), port, helper.Txt(self.request.method)))
        elif self.request.method:
            helper.printer("INFO", 'HTTP | %s:%s%s | %s | %s %s' % (
                helper.Txt(host), port, helper.Txt(self.request.build_url()), helper.Txt(self.request.method), helper.Txt(self.response.code), helper.Txt(self.response.reason)))

        return False

    def process(self):
        while True:
            rlist, wlist = [self.client.conn], []

            if self.client.has_buffer():
                wlist.append(self.client.conn)
            if self.server and not self.server.closed:
                rlist.append(self.server.conn)
            if self.server and not self.server.closed and self.server.has_buffer():
                wlist.append(self.server.conn)

            r, w, _ = select.select(rlist, wlist, [], 1)

            # handle writable list
            # client
            if self.client.conn in w:
                self.client.flush()

            # server
            if self.server and not self.server.closed and self.server.conn in w:
                self.server.flush()

            # handling client and server connection
            # client
            if self.client.conn in r:
                data = self.client.recv()
                self.last_activity = datetime.utcnow()

                if not data:
                    break

                try:
                    self.handle_request(data)
                except helper.ProxyConnFailed as e:
                    helper.printer("EXPT", e)
                    self.client.queue(helper.BAD_GATEWAY)
                    self.client.flush()
                    break

            # server
            if self.server and not self.server.closed and self.server.conn in r:
                data = self.server.recv()
                self.last_activity = datetime.utcnow()

                if not data:
                    self.server.close()
                else:
                    self.handle_response(data)

            # stop if response state is complete or the maximum inactivity reached
            if self.client.buffer_size() == 0 and \
                    (self.response.state == helper.HTTPStates.COMPLETE or
                     (datetime.utcnow() - self.last_activity).seconds > 30):
                break

    def run(self):
        try:
            self.process()
        except Exception as e:
            helper.printer("EXPT",
                           'Exception when handle connection %r: %r' % (self.client.conn, e))
        finally:
            self.client.close()
            self.log()


class ProxyServer(object):

    def __init__(self, hostname='127.0.0.1', port=12345, blacklist='blacklist.txt'):
        self.hostname = hostname
        self.port = port
        self.blacklist = blacklist

        self.socket = None

    def handle(self, client):
        proxy = Proxy(client, self.blacklist)
        proxy.daemon = True
        proxy.start()

    def run(self):
        try:
            helper.printer(
                "INFO", 'Starting proxy server on port %d' % self.port)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind((self.hostname, self.port))
            self.socket.listen(300)
            while True:
                conn, addr = self.socket.accept()
                client = connector.Client(conn, addr)
                self.handle(client)
        except Exception as e:
            helper.printer("EXPT", 'Exception when running the server: %r' % e)
        finally:
            helper.printer("INFO", 'Closing server socket...')
            self.socket.close()


def main():
    sys.tracebacklimit = 0
    signal.signal(signal.SIGINT, helper.signal_handler)
    proxy = ProxyServer()
    proxy.run()


if __name__ == '__main__':
    main()
