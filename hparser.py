import helper
import re
from urllib import parse as urlparse


class Parser(object):

    def __init__(self, handler_type):
        assert handler_type in (helper.HTTPTypes.REQUEST,
                                helper.HTTPTypes.RESPONSE)
        self.type = handler_type
        self.state = helper.HTTPStates.INITIALIZED

        self.method = None
        self.url = None
        self.code = None
        self.reason = None
        self.version = None

        self.cache = False
        self.cache_content = b''

        self.raw = b''
        self.buffer = b''

        self.headers = dict()
        self.body = None

    def parse(self, data):
        self.raw += data
        data = self.buffer + data
        self.buffer = b''

        while len(data) > 0:
            data = self.process(data)
        self.buffer = data

    def process(self, data):
        if self.state in (helper.HTTPStates.HEADERS_COMPLETE,
                          helper.HTTPStates.RCVING_BODY,
                          helper.HTTPStates.COMPLETE) and \
                (self.method == b'POST' or self.type == helper.HTTPTypes.RESPONSE):
            if not self.body:
                self.body = b''

            if b'Content-Length' in self.headers:
                self.state = helper.HTTPStates.RCVING_BODY
                self.body += data
                if len(self.body) >= int(self.headers[b'Content-Length'][1]):
                    self.state = helper.HTTPStates.COMPLETE

            if b'Cache-Control' in self.headers:
                cache_op = re.split(
                    ',\s*', helper.Txt(self.headers[b'Cache-Control'][1]))
                self.cache = not any(op in (
                    'max-age=0', 'no-cache', 'no-store', 'private', 's-maxage=0') for op in cache_op)
            return b''

        line, data = self.split(data)
        if line is False:
            return b''

        if self.state == helper.HTTPStates.INITIALIZED:
            # Processing status line
            line_l = line.split(b' ')
            if self.type == helper.HTTPTypes.REQUEST:
                self.method = line_l[0].upper()
                self.url = urlparse.urlsplit(line_l[1])
                self.version = line_l[2]
            else:
                self.version = line_l[0]
                self.code = line_l[1]
                self.reason = b' '.join(line_l[2:])
            self.state = helper.HTTPStates.LINE_RCVD

        elif self.state in (helper.HTTPStates.LINE_RCVD, helper.HTTPStates.RCVING_HEADERS):
            # Processing headers line
            if len(line) == 0:
                if self.state == helper.HTTPStates.RCVING_HEADERS:
                    self.state = helper.HTTPStates.HEADERS_COMPLETE
                elif self.state == helper.HTTPStates.LINE_RCVD:
                    self.state = helper.HTTPStates.RCVING_HEADERS
            else:
                self.state = helper.HTTPStates.RCVING_HEADERS
                parts = line.split(b':')
                key = parts[0].strip()
                value = b':'.join(parts[1:]).strip()
                self.headers[key] = (key, value)

        if self.is_state_complete(data):
            if b'Cache-Control' in self.headers:
                cache_op = re.split(
                    ',\s*', helper.Txt(self.headers[b'Cache-Control'][1]))
                self.cache = not any(
                    op in ('max-age=0', 'no-cache', 'no-store') for op in cache_op)

            self.state = helper.HTTPStates.COMPLETE

        return data

    def is_state_complete(self, data):
        # There are three cases to mark it as a complete response header:
        #
        #  1. HTTPS Request status line received
        #  2. HTTP Non-POST Request Headers Received
        #  3. HTTP POST Request Headers with no content Received
        return self.type == helper.HTTPTypes.REQUEST and \
            ((self.state == helper.HTTPStates.LINE_RCVD and self.method == b'CONNECT' and data == b'\r\n') or
             (self.state == helper.HTTPStates.HEADERS_COMPLETE and self.method != b'POST' and self.raw.endswith(b'\r\n' * 2)) or
                (self.state == helper.HTTPStates.HEADERS_COMPLETE and self.method == b'POST' and
                 (b'Content-Length' not in self.headers or (b'Content-Length' in self.headers and int(self.headers[b'Content-Length'][1]) == 0)) and
                 self.raw.endswith(b'\r\n' * 2)))

    def build_url(self):
        if not self.url:
            return b'/None'

        url = self.url.path
        if url == b'':
            url = b'/'
        if not self.url.query == b'':
            url += b'?' + self.url.query
        if not self.url.fragment == b'':
            url += b'#' + self.url.fragment
        return url

    def nreq(self):
        # for a request, modify before sending
        req = b' '.join([self.method, self.build_url(), self.version])
        req += b'\r\n'

        for k in self.headers:
            if k not in [b'Proxy-Connection', b'Connection', b'Keep-Alive']:
                req += self.headers[k][0] + b': ' + \
                    self.headers[k][1] + b'\r\n'

        for k in [(b'Connection', b'Close')]:
            req += k[0] + b': ' + k[1] + b'\r\n'

        req += b'\r\n'
        if self.body:
            req += self.body

        return req

    @staticmethod
    def split(data):
        pos = data.find(b'\r\n')
        if pos == -1:
            return False, data
        line = data[:pos]
        data = data[pos + len(b'\r\n'):]
        return line, data
